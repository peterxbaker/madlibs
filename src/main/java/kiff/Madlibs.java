package kiff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Madlibs {

    private static final String story = """
            The {noun} really wanted the {noun} and the {noun} to be {plural noun}
            So then the {noun} could {verb} {plural none}.
            """;

    public static void main(String[] args) {

        List<AtomicReference<String>> segments = new LinkedList<>();


        int closebrace = -1;
        int openbrace = story.indexOf("{");

        while (openbrace >= 0) {
            segments.add(new AtomicReference<>(story.substring(closebrace + 1, openbrace)));

            closebrace = story.indexOf("}", openbrace);
            segments.add(new AtomicReference<>(story.substring(openbrace, closebrace)));

            openbrace = story.indexOf("{", closebrace + 1);
        }

        segments.add(new AtomicReference<>(story.substring(closebrace + 1)));

        List<AtomicReference<String>> prompts = segments.stream()
                .filter(s -> s.get().startsWith("{"))
                .collect(Collectors.toCollection(ArrayList::new));

        Collections.shuffle(prompts);

        for (AtomicReference<String> prompt : prompts) {
            prompt.set(System.console().readLine("%s: ", prompt.get().substring(1)));
        }

        String finalStory = segments.stream()
                .map(AtomicReference::get)
                .collect(Collectors.joining());

        System.out.println();
        System.out.println(finalStory);

    }
}
